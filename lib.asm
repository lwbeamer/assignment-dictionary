section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy



 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60         
    syscall
      

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
      xor rax, rax
      .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
      .end:
      ret

; Принимает дескриптор и указатель на нуль-терминированную строку, выводит её в выбранный поток
print_string:
    push rsi
    call string_length
    mov     rdx, rax
    mov     rsi, rdi
    pop     rdi
    mov     rax, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
    


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:    
    mov r8, 10 
    push 0
    mov rax, rdi ;поgложили наше число в rax
	.loop:
		xor rdx, rdx
		div r8 ; в rdx попадает последняя цифра нашего числа
		add rdx, '0' ; прибавляем код символа 0, чтобы в сумме получить ASCII код нужной цифры
		push rdx
		cmp rax, 0
		jnz .loop
	.print:
		pop rdi ;достаём из стека по одной цифре и выводим их, пока не встретим завершающий символ 0xA
		cmp rdi, 0
		je .end
		call print_char
		jmp .print
	.end:
		ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - 1st rsi - 2nd
string_equals:

    xor r10, r10 ; счётчик
    xor r8, r8 ; регистр для символов первой строки
    xor r9, r9 ; регистр для символов второй строки

.loop:
    mov r8b, byte[rdi+r10]
    mov r9b, byte[rsi+r10]
    cmp r8b, r9b
    jne .error
    inc r10
    cmp r8b, 0 ; здесь на 0 можно проверить любой регистр, т.к. они равны
    jnz .loop
    mov rax, 1
    ret
.error:
    xor rax, rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0 ; пушим лбое число (в данном случае 0), чтобы сдвинуть вершину стрека
    mov rax, 0 ; sys_read 0
    mov rdi, 0 ; stdin descriptor
    mov rdx, 1 ; length = 1 sym
    mov rsi, rsp; rsi принимает адрес, прочитанный символ запишется на вершину стека
    syscall
    pop rax ; теперь прочитанный символ хранится в rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

;rdi - адрес начала буфера, rsi - размер
read_word:

    mov r8, rdi ; адрес начала буфера
    xor r9, r9 ; длина слова будет здесь
    mov r10, rdi; размер буфера
     
.loop:
    push r9
    push rsi
    call read_char
    pop rsi
    pop r9
    cmp al, 0
    je .end
    cmp al, 0x20
    je .check
    cmp al, 0x9
    je .check
    cmp al, 0xA
    je .check
    
    mov [r8], al  ; запись в буфер
    inc r8
    inc r9
    cmp r9, rsi
    jge .error
    jmp .loop
        
.error:
    mov rdx, 0
    mov rax, 0
    ret

.check:
    cmp r9, 0x0
    je .loop

.end:
    mov rax, r10
    mov rdx, r9
    mov byte[r8], 0 ;добавляем нуль-терминатор
    ret  
   

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi - указатель на строку
; 0x30 - код 0    0x39 - код 9
parse_uint:
    xor r8, r8
    xor rdx, rdx
    xor rax, rax
.loop:
    cmp byte [rdi + rdx], 0x30   ;
    jl .end			  ;	проверяем, является ли символ цифрой
    cmp byte [rdi + rdx], 0x39   ;
    jg .end			  ;
    mov r8b, byte [rdi + rdx]     
    inc rdx
    imul rax, 10 ; "сдвиг" влево на 1 разряд
    sub r8b, 0x30 ; получаем из ASCII кода непосредственно цифру
    add rax, r8
    jmp .loop
.end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .error
    inc rdx
    neg rax
    ret
.error:
    xor rax,rax
    xor rdx,rdx
    ret    


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi,rsi,rdx
string_copy:
    xor r10, r10
    cmp rdx, 0
    je .zerobuffer
    mov r10b, byte[rdi]
    mov byte[rsi], r10b
    dec rdx
    inc rdi
    inc rsi
    cmp r10, 0
    jne string_copy
    ret
    .zerobuffer:
    xor rax, rax
    ret 
