%include "lib.inc"

global find_word


section .text

;rdi - указатель на нуль-терм строку (на ключ)
;rsi- указатель на начало словаря
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.

find_word:
	push rsi
	add rsi, 8 
	call string_equals  ; string_equals принимает две строки rdi и rsi
	pop rsi
	cmp rax,1
	je .success
		
 	mov rsi, [rsi] ; помещаем в rsi адрес следующиего элемента
 	cmp rsi, 0
 	je .end
 				                          
	jmp find_word	
	
.success:
	mov rax, rsi 
	ret
.end:
	mov rax, 0
	ret


	


