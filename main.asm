section .rodata

%include "words.inc"
%include "lib.inc"

global _start
extern find_word

%define buff_size 256

section .data

errormes1: db "Некорректная строка на входе", 0
errormes2: db "Ключ не найден", 0

buffer: times 256 db 0 


section .text

;Читает строку размером не более 255 символов в буфер с stdin.
;Пытается найти вхождение в словаре; если оно найдено, распечатывает в stdout значение по этому ключу. 
;Иначе выдает сообщение об ошибке.

_start:

mov rdi, buffer ; это параметры для функции read_word
mov rsi, buff_size	; указатель на буффер и его максимальная длина по условию


call read_word
cmp rax, 0 ; если вернулся 0 - значит ошибка чтения
je .readerror


mov rdi, rax	; указатель на нуль-терм строку, который вернулся от функции read_word
mov rsi, dict_start ; адрес начала словаря. Определяется в colon.inc
call find_word 

cmp rax, 0
je .notfounderror


add rax, 8 ;пропускаем адрес следующего элемента
mov rdi, rax
call string_length
add rdi,rax ; пропускаем длину слова
add rdi, 1 ; добавляем 1, чтобы в rdi теперь лежал адрес значения
mov rsi, 1
call print_string
jmp  .end


.readerror:
	mov rdi, errormes1
	mov rsi, 2
	call print_string
	call exit
.notfounderror:
	mov rdi, errormes2
	mov rsi, 2
	call print_string
	call exit
.end:
	call exit

