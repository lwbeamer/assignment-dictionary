lib.o: lib.asm 
	nasm -f elf64 -o lib.o lib.asm

dict.o: dict.asm lib.inc
	nasm -f elf64 -o dict.o dict.asm

main.o: main.asm lib.inc colon.inc words.inc
	nasm -f elf64 -o main.o main.asm

program: main.o dict.o lib.o
	ld -o program main.o dict.o lib.o



